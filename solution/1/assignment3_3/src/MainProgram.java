import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class MainProgram {
    public static FileWriter fileWriter;

    public static void main(String[] args) throws IOException {

        fileWriter = new FileWriter("output1.txt");

        List<String> lines = Files.readAllLines(Paths.get("input.txt"));

        UberTree tree = new UberTree();

        fileWriter.write("--------------- Welcome to Uber Booking System ---------------\n");


        for (int i = 0; i < lines.size(); i++) {
            if (lines.get(i).matches(" +") || lines.get(i).isEmpty()) {
                continue;
            }
            String[] words = lines.get(i).split(" +");
            String command = words[0];
            if (!command.equals("Quit")) {
                if (command.equals("ADD_CAPTAIN")) {
                    fileWriter.write("Command Add_Captain:");
                } else if (!command.equals("DELETE_CAPTAIN")) {
                    fileWriter.write("Command " + command + ":");
                }
            }
            switch (command) {
                case "ADD_CAPTAIN":
                    tree.add_captain(Integer.parseInt(words[1]), words[2]);
                    break;
                case "BOOK_RIDE":
                    tree.book_ride(Integer.parseInt(words[1]));
                    break;
                case "DISPLAY_CAPTAIN_INFO":
                    tree.display_captain_by_ID(Integer.parseInt(words[1]));
                    break;
                case "FINISH_RIDE":
                    tree.finish_ride(Integer.parseInt(words[1]), Integer.parseInt(words[2]));
                    break;
                case "DISPLAY_ALL_CAPTAINS":
                    tree.display_all_captains_info();
                    break;
                case "DELETE_CAPTAIN":
                    tree.delete_captain(Integer.parseInt(words[1]));
                    break;
                case "Quit":
                    fileWriter.write("Thank you for using Uber System, Good Bye!\n");
                    fileWriter.flush();
                    fileWriter.close();
                    return;
                default:
                    System.out.println("Wrong command!");
                    break;
            }
        }


    }

}
