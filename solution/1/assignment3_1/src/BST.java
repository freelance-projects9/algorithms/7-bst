public class BST {

    BSTNode head;

    public BST(BSTNode head) {
        this.head = head;
    }

    public void add(int data) {
        head.add(data);
    }

    public void printBST() {
        head.printData();
    }

    public boolean ensureIsBalanced() {
        if(head == null){
            return true;
        }
        else if(head.leftChild == null || head.rightChild == null){
            return false;
        }
        int leftNodes = head.leftChild.countNodes();
        int rightNodes = head.rightChild.countNodes();
        return leftNodes == rightNodes;
    }

}
