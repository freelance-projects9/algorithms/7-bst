public class Main {


    public static void main(String[] args) {

        BST myBST = new BST(new BSTNode(12));
        myBST.add(4);
        myBST.add(2);
        myBST.add(8);
        myBST.add(10);
        myBST.add(9);
        myBST.add(11);
        myBST.add(16);
        myBST.add(14);
        myBST.add(18);
        myBST.add(20);
        myBST.add(21);
        myBST.add(22);
        myBST.printBST();
        boolean isBalanced = myBST.ensureIsBalanced();

        if(isBalanced){
            System.out.println("Balanced");
        }else{
            System.out.println("not Balanced");
        }

    }
}


class BSTNode {
    public BSTNode leftChild;
    public BSTNode rightChild;
    int data;

    public BSTNode(int data) {
        this.data = data;
    }

    public void add(int data) {
        if (data > this.data) {
            insertRight(data);
        } else {
            insertLeft(data);
        }
    }

    private void insertLeft(int data) {
        if (leftChild == null) {
            leftChild = new BSTNode(data);
        } else {
            leftChild.add(data);
        }
    }

    public void insertRight(int data) {
        if (rightChild == null) {
            rightChild = new BSTNode(data);
        } else {
            rightChild.add(data);
        }
    }

    public void printData() {
        if (leftChild != null) {
            leftChild.printData();
        }
        System.out.println("\t" + data);
        if (rightChild != null) {
            rightChild.printData();
        }
    }

    public int countNodes() {
        int nodes = 1;
        if(leftChild != null){
            nodes += leftChild.countNodes();
        }
        if(rightChild != null){
            nodes += rightChild.countNodes();
        }
        return nodes;
    }
}