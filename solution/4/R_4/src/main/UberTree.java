package main;

import java.io.IOException;

public class UberTree {

    private static final String ADD_NEW_CAPTAIN = " Add a new captain record in the System\n\n";
    private static final String NOT_FOUND = " Couldn't find any captain with ID number ";
    private static final String LINE = "----------------------------------------------------------------\n";
    private CaptainNode root;


    void add_captain(int ID, String name) throws IOException {
        MainProgram.fileWriter.write(ADD_NEW_CAPTAIN);
        CaptainNode node = new CaptainNode(ID, name);
        if (isEmpty()) {
            write_node_info(node);
            root = node;
            return;
        }
        root.addChild(node);
        write_node_info(node);
    }

    private boolean isEmpty() {
        return root == null;
    }

    private void write_node_info(CaptainNode node) throws IOException {
        String availableStr = node.isAvailable() ? "True " : "False ";
        String out = "\t\t\tID: " + node.getId() + "\n" +
                "\t\t\tName: " + node.getName() + "\n" +
                "\t\t\tAvailable: " + availableStr + "\n" +
                "\t\t\tRating star: " + node.getRatting() + "\n\t\n" +
                LINE;
        MainProgram.fileWriter.write(out);
    }

    void display_all_captains_info() throws IOException {
        MainProgram.fileWriter.write("\n\n");
        displayTreeInfo(root);
    }

    public void displayTreeInfo(CaptainNode node) throws IOException {
        MainProgram.fileWriter.write("\n");
        String availableStr = node.isAvailable() ? "True " : "False ";
        String info = "\t\t\tID: " + node.getId() + "\n" +
                "\t\t\tName: " + node.getName() + "\n" +
                "\t\t\tAvailable: " + availableStr + "\n" +
                "\t\t\tRating star: " + node.getRatting() + "\n" + "\t\t\t" + "\n" +
                LINE;
        MainProgram.fileWriter.write(info);

        if (node.getLeft() != null) {
            displayTreeInfo(node.getLeft());
        }
        if (node.getRight() != null) {
            displayTreeInfo(node.getRight());
        }
    }

    void display_captain_by_ID(int ID) throws IOException {
        if (isEmpty()) {
            MainProgram.fileWriter.write("\nEmpty");
        }
        CaptainNode captain = getCaptain(ID);
        if (captain != null) {
            MainProgram.fileWriter.write("\n");
            captain.displayInfo();
            return;
        }
        MainProgram.fileWriter.write(NOT_FOUND + ID + "\n\n" +
                LINE);
    }

    void book_ride(int ID) throws IOException {
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.fileWriter.write(NOT_FOUND + ID + "\n\n" +
                    LINE);
            return;
        }
        if (node.book_captain()) {
            MainProgram.fileWriter.write(" Book a new Ride with captin " + ID + "\n\n" +
                    LINE);
        } else {
            MainProgram.fileWriter.write(" The captain " + node.getName() + " is not available. He is on another ride!\n\n" +
                    LINE);
        }
    }

    void finish_ride(int ID, int satisfaction) throws IOException {
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.fileWriter.write(NOT_FOUND + ID + "\n\n" +
                    LINE);
            return;
        }
        if (node.isAvailable()) {
            MainProgram.fileWriter.write(" The  captain " + node.getName() + " is not in a ride!\n" +
                    LINE);
            return;
        }
        node.rate(satisfaction);
        node.setAvailable(true);
        MainProgram.fileWriter.write(" Finish ride with captin " + ID + "\n\n");
        node.displayInfo();
    }




    public CaptainNode deleteNode(CaptainNode root, int ID) {
        if (isEmpty()) return null;

        if (ID > root.getId()) {
            root.setRight(deleteNode(root.getRight(), ID));
        } else if (ID < root.getId()) {
            root.setLeft(deleteNode(root.getLeft(), ID));
        } else {
            if (root.getLeft() == null && root.getRight() == null) {
                root = null;
            } else if (root.getRight() != null) {
                root.copy(smallestNode(root.getRight()));
                root.setRight(deleteNode(root.getRight(), root.getId()));
            } else {
                root.copy(biggestNode(root.getLeft()));
                root.setLeft(deleteNode(root.getLeft(), root.getId()));
            }
        }
        return root;
    }

    private CaptainNode biggestNode(CaptainNode node) {
        while(node.getRight()!=null){
            node = node.getRight();
        }
        return node;
    }

    private CaptainNode smallestNode(CaptainNode node) {
        while(node.getLeft() != null){
            node = node.getLeft();
        }
        return node;
    }
    void delete_captain(int ID) throws IOException {
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.fileWriter.write("Command DELETE_CAPTAIN:" + NOT_FOUND + ID + "\n\n" +
                    LINE);
            return;
        }
        String name = node.getName();
        root = deleteNode(root, ID);
        MainProgram.fileWriter.write("Command  DELETE_CAPTAIN:The captain " + name + " left Uber\n" +
                LINE);

    }

    CaptainNode getCaptain(int ID) {

        CaptainNode temp = root;
        while (temp != null) {
            if (ID == temp.getId()) {
                return temp;
            }
            if (ID > temp.getId()) {
                temp = temp.getRight();
            } else {
                temp = temp.getLeft();
            }
        }
        return null;
    }

}
