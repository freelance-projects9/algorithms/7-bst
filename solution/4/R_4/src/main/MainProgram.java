package main;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class MainProgram {
    public static FileWriter fileWriter;
    private static List<String> lines;
    private static UberTree tree;

    public static void main(String[] args) throws IOException {

        initializeFileWriter("output.txt");

        readInputs("input.txt");

        initializeTree();

        writeWelcomePhrase();

        for (String line : lines) {
            if (!isEmptyLine(line)) {
                String regex = " +";
                String[] words = line.split(regex);
                String command = words[0];
                switch (command) {
                    case "ADD_CAPTAIN":
                        fileWriter.write("Command Add_Captain:");
                        tree.add_captain(Integer.parseInt(words[1]), words[2]);
                        break;
                    case "BOOK_RIDE":
                        fileWriter.write("Command " + command + ":");
                        tree.book_ride(Integer.parseInt(words[1]));
                        break;
                    case "DISPLAY_CAPTAIN_INFO":
                        fileWriter.write("Command " + command + ":");
                        tree.display_captain_by_ID(Integer.parseInt(words[1]));
                        break;

                    case "DELETE_CAPTAIN":
                        tree.delete_captain(Integer.parseInt(words[1]));
                        break;
                    case "FINISH_RIDE":
                        fileWriter.write("Command " + command + ":");
                        tree.finish_ride(Integer.parseInt(words[1]), Integer.parseInt(words[2]));
                        break;
                    case "DISPLAY_ALL_CAPTAINS":
                        fileWriter.write("Command " + command + ":");
                        tree.display_all_captains_info();
                        break;
                    case "Quit":
                        fileWriter.write("Thank you for using Uber System, Good Bye!\n");
                        fileWriter.flush();
                        fileWriter.close();
                        return;
                    default:
                        System.out.println("Wrong command!");
                        break;
                }
            }
        }


    }

    private static void initializeTree() {
        tree = new UberTree();
    }

    private static boolean isEmptyLine(String line) {
        return line.matches(" +") || line.isEmpty();
    }

    private static void writeWelcomePhrase() throws IOException {
        fileWriter.write("--------------- Welcome to Uber Booking System ---------------\n");
    }

    private static void initializeFileWriter(String path) {
        try {
            fileWriter = new FileWriter(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private static void readInputs(String path) {
        try {
            lines = Files.readAllLines(Paths.get(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}
