package main;

import java.io.IOException;

public class CaptainNode {

    private int captainID;
    private String CaptainName;
    private int star;
    private boolean available;
    private CaptainNode right;
    private CaptainNode left;


    CaptainNode(int captainID, String CaptainName) {
        this.CaptainName = CaptainName;
        this.captainID = captainID;
        this.star = 0;
        this.available = true;
    }

    public void addChild(CaptainNode captainNode) {
        if (this.captainID <= captainNode.captainID) {
            addRightChild(captainNode);
        } else {
            addLeftChild(captainNode);
        }
    }

    private void addRightChild(CaptainNode captainNode) {
        if (right != null) {
            right.addChild(captainNode);
            return;
        }
        right = captainNode;
    }

    private void addLeftChild(CaptainNode captainNode) {
        if (left != null) {
            left.addChild(captainNode);
            return;
        }
        left = captainNode;
    }



    public void displayInfo() throws IOException {
        String availableStr = this.available ? "True " : "False ";

        String info = "\t\t\tID: " + captainID + "\n" +
                "\t\t\tName: " + CaptainName + "\n" +
                "\t\t\tAvailable: " + availableStr + "\n" +
                "\t\t\tRating star: " + star + "\n" + "\t" + "\n" +
                "----------------------------------------------------------------\n";
        MainProgram.fileWriter.write(info);
    }


    public int getId() {
        return captainID;
    }

    public CaptainNode getRight() {
        return right;
    }

    public CaptainNode getLeft() {
        return left;
    }


    public boolean book_captain() {
        if (available) {
            available = false;
            return true;
        }
        return false;

    }

    public void rate(int satisfaction) {
        star += satisfaction;
        if (star > 5)
            star = 5;
        if (star < 0)
            star = 0;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setLeft(CaptainNode left) {
        this.left = left;
    }

    public void setRight(CaptainNode right) {
        this.right = right;
    }

    public String getName() {
        return CaptainName;
    }

    public int getRatting() {
        return star;
    }

    public void copy(CaptainNode captainNode) {
        star = captainNode.getRatting();
        CaptainName = captainNode.getName();
        available = captainNode.isAvailable();
        captainID = captainNode.getId();
    }
}
