package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MainProgram {
    public static PrintWriter writer;
    private static Scanner scanner;
    private static UberTree tree;

    public static void main(String[] args) throws IOException {

        writer = new PrintWriter("output.txt");
        scanner = new Scanner(new File("input.txt"));

        tree = new UberTree();

        printWelcome();

        while (scanner.hasNext()) {
            String command = scanner.next();
            switch (command) {
                case "ADD_CAPTAIN":
                    addCaptain();
                    break;
                case "BOOK_RIDE":
                    writer.write("Command " + command + ":");
                    bookRide();
                    break;
                case "DISPLAY_CAPTAIN_INFO":
                    writer.write("Command " + command + ":");
                    displayCaptainInfo();
                    break;
                case "FINISH_RIDE":
                    writer.write("Command " + command + ":");
                    finishRide();
                    break;
                case "DISPLAY_ALL_CAPTAINS":
                    writer.write("Command " + command + ":");
                    tree.display_all_captains_info();
                    break;
                case "DELETE_CAPTAIN":
                    deleteCaptain();
                    break;
                case "Quit":
                    writer.write("Thank you for using Uber System, Good Bye!\n");
                    writer.flush();
                    writer.close();
                    return;
            }
        }

    }

    private static void printWelcome() {
        String welcome = "--------------- Welcome to Uber Booking System ---------------\n";
        writer.write(welcome);
    }

    private static void deleteCaptain() throws IOException {
        int ID = Integer.parseInt(scanner.next());
        tree.delete_captain(ID);

    }

    private static void finishRide() throws IOException {
        int ID = Integer.parseInt(scanner.next());
        int satisfaction = Integer.parseInt(scanner.next());
        tree.finish_ride(ID, satisfaction);
    }

    private static void displayCaptainInfo() throws IOException {
        int ID = Integer.parseInt(scanner.next());
        tree.display_captain_by_ID(ID);

    }

    private static void bookRide() throws IOException {
        int ID = Integer.parseInt(scanner.next());
        tree.book_ride(ID);
    }

    private static void addCaptain() throws IOException {
        writer.write("Command Add_Captain:");
        int ID = Integer.parseInt(scanner.next());
        String name = scanner.next();
        tree.add_captain(ID, name);
    }

}
