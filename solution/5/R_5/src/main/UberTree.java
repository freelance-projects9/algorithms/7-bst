package main;

import java.io.IOException;

public class UberTree {

    private CaptainNode root;


    void add_captain(int ID, String name) throws IOException {
        MainProgram.writer.write(" Add a new captain record in the System\n\n");
        CaptainNode node;
        if (isEmpty()) {
            node = new CaptainNode(ID, name);
            root = node;

        } else {
            node = new CaptainNode(ID, name);
            addChild(root, node);
        }
        String info = "\t\t\tID: " + node.getId() + "\n" +
                "\t\t\tName: " + node.getName() + "\n" +
                "\t\t\tAvailable: " + node.getAvailable() + "\n" +
                "\t\t\tRating star: " + node.getStar() + "\n\t\n" +
                "----------------------------------------------------------------\n";
        MainProgram.writer.write(info);
    }

    public void addChild(CaptainNode root, CaptainNode node2add) {
        if (root.getId() > node2add.getId() && !root.hasLeft()) {
            root.setLeft(node2add);
        } else if (root.getId() > node2add.getId()) {
            addChild(root.getLeft(), node2add);
        } else if (!root.hasRight()) {
            root.setRight(node2add);
        } else {
            addChild(root.getRight(), node2add);
        }
    }

    private boolean isEmpty() {
        return root == null;
    }

    void display_all_captains_info() throws IOException {
        MainProgram.writer.write("\n\n");
        root.recPrintTree();
    }

    void display_captain_by_ID(int ID) throws IOException {
        if (root == null) {
            MainProgram.writer.write("\nEmpty");
        }
        CaptainNode captain = getCaptain(ID);
        if (captain != null) {
            MainProgram.writer.write("\n");
            captain.displayInfo(1);
            return;
        }
        MainProgram.writer.write(" Couldn't find any captain with ID number " + ID + "\n\n" +
                "----------------------------------------------------------------\n");
    }

    void book_ride(int ID) throws IOException {
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.writer.write(" Couldn't find any captain with ID number " + ID + "\n\n" +
                    "----------------------------------------------------------------\n");
            return;
        }

        if (node.isAvailable()) {
            node.setAvailable(false);
            MainProgram.writer.write(" Book a new Ride with captin " + ID + "\n\n" +
                    "----------------------------------------------------------------\n");
        } else {
            MainProgram.writer.write(" The captain Shahid_Aman is not available. He is on another ride!\n\n" +
                    "----------------------------------------------------------------\n");
        }
    }

    void finish_ride(int ID, int satisfaction) throws IOException {
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.writer.write(" Couldn't find any captain with ID number " + ID + "\n\n" +
                    "----------------------------------------------------------------\n");
            return;
        }
        if (node.isAvailable()) {
            MainProgram.writer.write(" The  captain " + node.getName() + " is not in a ride!\n" +
                    "----------------------------------------------------------------\n");
            return;
        }
        node.rate(satisfaction);
        node.setAvailable(true);
        MainProgram.writer.write(" Finish ride with captin " + ID + "\n\n");
        node.displayInfo(1);
    }



    public CaptainNode deleteNode(CaptainNode root, int ID) {
        if (root == null) return null;
        if (ID == root.getId()) {
            if (root.getLeft() == null && root.getRight() == null) {
                root = null;
            } else if (root.getRight() == null) {
                CaptainNode node = getAlternative(root, "left");
                root.copyInfo(node.getId(), node.getName(), node.isAvailable(), node.getStar());
                root.setLeft(deleteNode(root.getLeft(), root.getId()));
            } else {
                CaptainNode node = getAlternative(root, "right");
                root.copyInfo(node.getId(), node.getName(), node.isAvailable(), node.getStar());
                root.setRight(deleteNode(root.getRight(), root.getId()));
            }
        } else if (ID > root.getId()) {
            root.setRight(deleteNode(root.getRight(), ID));
        } else {
            root.setLeft(deleteNode(root.getLeft(), ID));
        }
        return root;
    }

    private CaptainNode getAlternative(CaptainNode root, String side) {
        if(side.equals("right")){
            root = root.getRight();
            while (root.getLeft() != null) {
                root = root.getLeft();
            }
        }else{
            root = root.getLeft();
            while (root.getRight() != null) {
                root = root.getRight();
            }
        }
        return root;
    }


    void delete_captain(int ID) throws IOException {
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.writer.write("Command DELETE_CAPTAIN: Couldn't find any captain with ID number " + ID + "\n\n" +
                    "----------------------------------------------------------------\n");
            return;
        }
        String name = node.getName();
        root = deleteNode(root, ID);
        MainProgram.writer.write("Command  DELETE_CAPTAIN:The captain " + name + " left Uber\n" +
                "----------------------------------------------------------------\n");

    }

    CaptainNode getCaptain(int ID) {
        CaptainNode temp = root;
        while (temp != null) {
            if (ID == temp.getId()) {
                return temp;
            }
            if (ID > temp.getId()) {
                temp = temp.getRight();
            } else {
                temp = temp.getLeft();
            }
        }
        return null;
    }

}
