import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Test {

    public static void main(String[] args) throws IOException {
        MainProgram.main(args);
        test(1);
    }

    private static void test(int i) {
        Boolean result = null;
        try {
            result = testOutput();
        } catch (Exception e) {
            System.out.println("Main" + i + ": Test failed with exception");
            return;
        }
        if (result) {
            System.out.println("Main" + i + ": Test is Passed!");
        } else {
            System.out.println("Main" + i + ": Test failed");
        }
    }

    private static Boolean testOutput() throws IOException {
        String path1 = "output.txt";
        String path2 = "output1.txt";
        List<String> output1 = Files.readAllLines(Paths.get(path1));
        List<String> output2 = Files.readAllLines(Paths.get(path2));

        if (output1.size() != output2.size()) {
            System.out.println(output1.size());
            System.out.println(output2.size());
            return false;
        }
        for (int i = 0; i < output2.size(); i++) {
            if (!output1.get(i).equals(output2.get(i))) {
                System.out.println("Line : " + i);
                System.out.println(output1.get(i));
                System.out.println(output2.get(i));
                return false;
            }
        }
        return true;
    }
}
