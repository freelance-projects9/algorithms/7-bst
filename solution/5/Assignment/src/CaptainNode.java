import java.io.IOException;

public class CaptainNode {

    private int captainID;
    private String CaptainName;
    private int star;
    private boolean available;
    private CaptainNode right;
    private CaptainNode left;


    CaptainNode(int ID, String name) {
        this.CaptainName = name;
        this.captainID = ID;
        star = 0;
        available = true;
    }



    public void recPrintTree() throws IOException {
        MainProgram.writer.write("\n\t\t\tID: " + captainID + "\n" +
                "\t\t\tName: " + CaptainName + "\n" +
                "\t\t\tAvailable: " + getAvailable() + "\n" +
                "\t\t\tRating star: " + star + "\n\t\t\t\n" +
                "----------------------------------------------------------------\n");
        if (left != null) {
            left.recPrintTree();
        }
        if (right != null) {
            right.recPrintTree();
        }
    }

    public void displayInfo(int i) throws IOException {
        String space = i == 1 ? "\t" : "\t\t\t";
        String info = "\t\t\tID: " + captainID + "\n" +
                "\t\t\tName: " + CaptainName + "\n" +
                "\t\t\tAvailable: " + getAvailable() + "\n" +
                "\t\t\tRating star: " + star + "\n" + space + "\n" +
                "----------------------------------------------------------------\n";
        MainProgram.writer.write(info);
    }

    public CaptainNode getRight() {
        return right;
    }

    public CaptainNode getLeft() {
        return left;
    }

    public int getId() {
        return captainID;
    }


    public void rate(int satisfaction) {
        star += satisfaction;
        checkRate();
    }

    private void checkRate() {
        star = Math.max(0, star);
        star = Math.min(5, star);
    }


    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setLeft(CaptainNode left) {
        this.left = left;
    }

    public void setRight(CaptainNode right) {
        this.right = right;
    }


    public int getStar(){
        return star;
    }

    public String getName() {
        return CaptainName;
    }

    public String getAvailable() {
        return available ? "True " : "False ";
    }

    public void copyInfo(int captainID, String name, boolean available, int star) {
        this.captainID = captainID;
        this.CaptainName = name;
        this.available = available;
        this.star = star;
    }

    public boolean hasLeft() {
        return left != null;
    }

    public boolean hasRight() {
        return right != null;
    }
}
