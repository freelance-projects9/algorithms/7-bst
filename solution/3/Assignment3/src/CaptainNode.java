import java.io.IOException;

public class CaptainNode {

    private int captainID;
    private String CaptainName;
    private int star;
    private boolean available;
    private CaptainNode right;
    private CaptainNode left;


    CaptainNode(int ID, String name) {
        CaptainName = name;
        captainID = ID;
        star = 0;
        available = true;
    }

    public void addChild(CaptainNode captainNode) {
        if (captainID > captainNode.captainID) {
            addLeft(captainNode);
        } else {
            addRight(captainNode);
        }

    }

    private void addRight(CaptainNode captainNode) {
        if (right != null) {
            right.addChild(captainNode);
            return;
        }
        // if right is null, set the new node to right variable
        right = captainNode;

    }

    private void addLeft(CaptainNode captainNode) {
        if (left != null) {
            left.addChild(captainNode);
            return;
        }
        // if left is null, set the new node to left variable
        left = captainNode;
    }


    public void displayTreeInfo() throws IOException {
        String info = "\n\t\t\tID: " + captainID + "\n";
        info += "\t\t\tName: " + CaptainName + "\n";
        info += "\t\t\tAvailable: " + getAvailable() + "\n";
        info += "\t\t\tRating star: " + star + "\n" + "\t\t\t" + "\n";
        info += "----------------------------------------------------------------\n";
        MainProgram.writer.write(info);
        if (left != null) {
            left.displayTreeInfo();
        }
        if (right != null) {
            right.displayTreeInfo();
        }
    }

    // print info for current node
    public void displayInfo() throws IOException {
        String info = getInfo();
        MainProgram.writer.write(info);
    }

    private String getInfo() {
        String info = "\t\t\tID: " + captainID + "\n" +
                "\t\t\tName: " + CaptainName + "\n" +
                "\t\t\tAvailable: " + getAvailable() + "\n" +
                "\t\t\tRating star: " + star + "\n" + "\t" + "\n" +
                "----------------------------------------------------------------\n";
        return info;
    }

    public int getId() {
        return captainID;
    }

    public CaptainNode getRight() {
        return right;
    }

    public CaptainNode getLeft() {
        return left;
    }


    public void rate(int satisfaction) {
        star += satisfaction;
        // to keep stars in the range [0,5]
        star = Math.min(star, 5);
        star = Math.max(star, 0);
    }


    public void finish_ride() {
        available = false;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setLeft(CaptainNode left) {
        this.left = left;
    }

    public void setRight(CaptainNode right) {
        this.right = right;
    }

    public String getName() {
        return CaptainName;
    }

    public String getAvailable() {
        return available ? "True " : "False ";
    }

    // copy node info to current node
    public void copy(CaptainNode node) {
        captainID = node.captainID;
        CaptainName = node.getName();
        available = node.isAvailable();
        star = node.star;
    }
}
