/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Main;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        // initialize output file writer
        FileWriter writer = new FileWriter("outputResults.txt");

        // initialize input scanner
        Scanner scanner = new Scanner(System.in);
        // read file name from user
        System.out.print("Enter Traces file name?: ");
        String fileName = scanner.nextLine();

        // read all lines in the input file
        List<String> lines = Files.readAllLines(Paths.get(fileName));

        System.out.println("DONE Reading");

        int size = lines.size();

        // for each line do:
        for (int i = 0; i < size; i++) {
            int j = i + 1;
            while (j < size) {
                // calculate euclidean distance between number in the line i and all numbers after it
                double dist = euclidean_dist(lines.get(i), lines.get(j));
                dist = round(dist);
                // write result to the output file
                writer.write(dist + "\n");
                j++;
            }
        }
        writer.close();
        System.out.println("The output results are stored in file outputResults.txt");
    }

    private static double round(double dist) {
        // round result number
        BigDecimal bd = new BigDecimal(Double.toString(dist));
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    private static double euclidean_dist(String point1, String point2) {
        double dist = 0;
        String[] p1 = point1.split(" ");
        String[] p2 = point2.split(" ");
        for (int i = 0; i < p1.length; i++) {
            int x1 = Integer.parseInt(p1[i]);
            int x2 = Integer.parseInt(p2[i]);
            dist += Math.pow((x1 - x2), 2);
        }
        dist = Math.sqrt(dist);
        return dist;
    }
}