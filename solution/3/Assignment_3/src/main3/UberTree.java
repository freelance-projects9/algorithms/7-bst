package main3;

import java.io.IOException;

public class UberTree {

    private CaptainNode root;

    private String notFoundStr = " Couldn't find any captain with ID number ";
    private String addedStr = " Add a new captain record in the System\n\n";
    private String lineStr = "----------------------------------------------------------------\n";


    void add_captain(int ID, String name) throws IOException {
        MainProgram.writer.write(addedStr);
        if (isEmpty()) {
            CaptainNode node = new CaptainNode(ID, name);
            root = node;
            node.displayInfo();
        } else {
            CaptainNode node = new CaptainNode(ID, name);
            root.addChild(node);
            node.displayInfo();
        }
    }

    private boolean isEmpty() {
        return root == null;
    }

    void display_all_captains_info() throws IOException {
        MainProgram.writer.write("\n\n");
        root.displayTreeInfo();
    }

    private void writeLines(int n) throws IOException {
        String lines = "";
        int j = 0;
        while (j < n) {
            lines += "\n";
            j++;
        }
        MainProgram.writer.write(lines);
    }

    void display_captain_by_ID(int ID) throws IOException {
        // get the needed node
        CaptainNode captain = getCaptain(ID);
        // node not found case
        if (captain == null) {
            MainProgram.writer.write(notFoundStr + ID + "\n\n" +
                    lineStr);
        } else {
            writeLines(1);
            captain.displayInfo();
        }
    }

    void book_ride(int ID) throws IOException {
        // get the needed node
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.writer.write(notFoundStr + ID + "\n\n" +
                    lineStr);
            return;
        } else if (node.isAvailable()) {
            node.setAvailable(false);
            MainProgram.writer.write(" Book a new Ride with captin " + ID + "\n\n" +
                    lineStr);
            return;
        }
        MainProgram.writer.write(" The captain " + node.getName() + " is not available. He is on another ride!\n\n" +
                lineStr);
    }

    void finish_ride(int ID, int satisfaction) throws IOException {
        // get the needed node
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.writer.write(notFoundStr + ID + "\n\n" +
                    lineStr);
        } else if (node.isAvailable()) {
            MainProgram.writer.write(" The  captain " + node.getName() + " is not in a ride!\n" +
                    lineStr);
        } else {
            // add user rate
            node.rate(satisfaction);
            // set the node available to ture
            node.setAvailable(true);
            MainProgram.writer.write(" Finish ride with captin " + ID + "\n\n");
            node.displayInfo();
        }
    }


    public CaptainNode deleteNode(CaptainNode root, int ID) {
        if (isEmpty()) return null;
        // the node is on the right subtree
        if (ID > root.getId()) {
            CaptainNode right = deleteNode(root.getRight(), ID);
            root.setRight(right);
        }
        // the node is on the left subtree
        else if (ID < root.getId()) {
            CaptainNode left = deleteNode(root.getLeft(), ID);
            root.setLeft(left);
        } else {
            // delete procedure
            root = doDelete(root);
        }
        return root;
    }

    private CaptainNode doDelete(CaptainNode root) {
        // leaf case
        if (root.getLeft() == null && root.getRight() == null) {
            root = null;
        }
        // no leaf
        else if (root.getRight() != null) {
            // it has right child

            // get the bigger node from the right subtree
            CaptainNode temp = root.getRight();
            while (temp.getLeft() != null) {
                temp = temp.getLeft();
            }

            // copy node data to the root
            root.copy(temp);
            // continue with the right subtree to remove the leaf node and return the new subtree
            // as the right child
            root.setRight(deleteNode(root.getRight(), root.getId()));
        } else {

            // get the smaller node in the tree (in the left subtree)
            CaptainNode temp = root.getLeft();
            while (temp.getRight() != null) {
                temp = temp.getRight();
            }

            // copy node data to the root
            root.copy(temp);
            // continue with the left subtree to remove the leaf node and return the new subtree
            // as the left child
            root.setLeft(deleteNode(root.getLeft(), root.getId()));
        }
        return root;
    }

    void delete_captain(int ID) throws IOException {

        // get the needed node
        CaptainNode temp = root;
        while (temp != null) {
            if (ID == temp.getId()) {
                break;
            }
            if (ID > temp.getId()) {
                temp = temp.getRight();
            } else {
                temp = temp.getLeft();
            }
        }
        // not found case
        if (temp == null) {
            MainProgram.writer.write("Command DELETE_CAPTAIN:" + notFoundStr + ID + "\n\n" +
                    lineStr);
            return;
        }

        String name = temp.getName();
        // start deletion
        root = deleteNode(root, ID);
        MainProgram.writer.write("Command  DELETE_CAPTAIN:The captain " + name + " left Uber\n" +
                lineStr);
    }

    CaptainNode getCaptain(int ID) {
        CaptainNode temp = root;
        while (temp != null) {
            if (ID == temp.getId()) {
                return temp;
            }
            if (ID > temp.getId()) {
                temp = temp.getRight();
            } else {
                temp = temp.getLeft();
            }
        }
        return null;
    }

}
