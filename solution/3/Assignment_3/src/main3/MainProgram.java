package main3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class MainProgram {

    // define input path
    private static String inputFilePath = "input.txt";

    // define output path
    private static String outputFilePath = "output.txt";

    // declare print writer
    public static PrintWriter writer;


    // Constant phrases
    private static String welcomePhrase = "--------------- Welcome to Uber Booking System ---------------\n";
    private static String goodByePhrase = "Thank you for using Uber System, Good Bye!\n";

    // declare UberTree object
    private static UberTree tree;

    public static void main(String[] args) throws IOException {

        // initialize print writer
        writer = new PrintWriter(outputFilePath);

        // initialize input reader
        File inputFile = new File(inputFilePath);
        Scanner inputReader = new Scanner(inputFile);

        // initialize UberTree object
        tree = new UberTree();

        // print welcome
        writer.write(welcomePhrase);

        // read inputs
        String line;
        while (inputReader.hasNextLine()) {
            line = inputReader.nextLine();
            if (!lineIsEmpty(line)) {

                // split line
                String[] words = line.split(" +");

                // run commands
                runCommand(words);
            }
        }
    }

    private static void runCommand(String[] words) throws IOException {

        switch (words[0]) {
            case "ADD_CAPTAIN":
                writer.write("Command Add_Captain:");
                tree.add_captain(Integer.parseInt(words[1]), words[2]);
                break;
            case "BOOK_RIDE":
                writer.write("Command " + words[0] + ":");
                tree.book_ride(Integer.parseInt(words[1]));
                break;
            case "DISPLAY_CAPTAIN_INFO":
                writer.write("Command " + words[0] + ":");
                tree.display_captain_by_ID(Integer.parseInt(words[1]));
                break;
            case "FINISH_RIDE":
                writer.write("Command " + words[0] + ":");
                tree.finish_ride(Integer.parseInt(words[1]), Integer.parseInt(words[2]));
                break;
            case "DISPLAY_ALL_CAPTAINS":
                writer.write("Command " + words[0] + ":");
                tree.display_all_captains_info();
                break;
            case "DELETE_CAPTAIN":
                tree.delete_captain(Integer.parseInt(words[1]));
                break;
            case "Quit":
                writer.write(goodByePhrase);
                writer.flush();
                writer.close();
        }
    }

    private static boolean lineIsEmpty(String line) {
        return line.matches(" +") || line.isEmpty();
    }

}
