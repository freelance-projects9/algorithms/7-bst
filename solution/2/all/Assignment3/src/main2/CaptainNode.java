package main2;

import java.io.IOException;

public class CaptainNode {

    private int captainID;
    private String CaptainName;
    private int star;
    private boolean available;
    private CaptainNode right;
    private CaptainNode left;


    CaptainNode(int ID, String name) {
        CaptainName = name;
        captainID = ID;
        star = 0;
        available = true;
    }

    public void addChild(CaptainNode captainNode) {
        if (captainID > captainNode.captainID) {
            if (left != null) {
                left.addChild(captainNode);
		return;
            }
            left = captainNode;
            return;
        }
        if (right != null) {
            right.addChild(captainNode);

        } else {
            right = captainNode;

        }

    }

    private void writeLines(int n) throws IOException {
        String lines = "";
        for (int j = 0; j < n; j++) {
            lines += "\n";
        }
        MainProgram.fileWriter.write(lines);
    }

    public void displayTreeInfo() throws IOException {
        writeLines(1);
        String info = "\t\t\tID: " + captainID + "\n" +
                "\t\t\tName: " + CaptainName + "\n" +
                "\t\t\tAvailable: " + getAvailable() + "\n" +
                "\t\t\tRating star: " + star + "\n" + "\t\t\t" + "\n" +
                "----------------------------------------------------------------\n";
        MainProgram.fileWriter.write(info);
        if (left != null) {
            left.displayTreeInfo();
        }
        if (right != null) {
            right.displayTreeInfo();
        }
    }

    public void displayInfo() throws IOException {
        String info = "\t\t\tID: " + captainID + "\n" +
                "\t\t\tName: " + CaptainName + "\n" +
                "\t\t\tAvailable: " + getAvailable() + "\n" +
                "\t\t\tRating star: " + star + "\n" + "\t" + "\n" +
                "----------------------------------------------------------------\n";
        MainProgram.fileWriter.write(info);
    }


    public int getId() {
        return captainID;
    }

    public CaptainNode getRight() {
        return right;
    }

    public CaptainNode getLeft() {
        return left;
    }

    public void book_captain() {
        available = false;
    }

    public void rate(int satisfaction) {
        star += satisfaction;
        checkStars();
    }

    private void checkStars() {
        if (star > 5)
            star = 5;
        else if (star < 0)
            star = 0;
    }

    public void finish_ride() {
        available = false;
    }

    public boolean isAvailable() {

        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setLeft(CaptainNode left) {
        this.left = left;
    }

    public void setRight(CaptainNode right) {
        this.right = right;
    }


    public String getName() {
        return CaptainName;
    }

    public String getAvailable() {
        return available ? "True " : "False ";
    }

    public int getRatting() {
        return star;
    }

    public void copy(CaptainNode successor) {
        captainID = successor.captainID;
        CaptainName = successor.getName();
        available = successor.isAvailable();
        star = successor.star;
    }
}
