package main2;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class MainProgram {
    public static FileWriter fileWriter;

    static List<String> lines;

    static UberTree tree;
    private static String welcomePhrase = "--------------- Welcome to Uber Booking System ---------------\n";
    private static String goodByePhrase = "Thank you for using Uber System, Good Bye!\n";

    public static void main(String[] args) throws IOException {

        fileWriter = new FileWriter("output.txt");

        lines = Files.readAllLines(Paths.get("input.txt"));

        tree = new UberTree();

        fileWriter.write(welcomePhrase);
        int n = lines.size();

        for (int i = 0; i < n; i++) {
            if (lineIsEmpty(lines.get(i))) {
                continue;
            }
            String spaceRegEx = " +";
            String[] words = lines.get(i).split(spaceRegEx);

            if (words[0].equals("ADD_CAPTAIN")) {
                fileWriter.write("Command Add_Captain:");
            } else if (!words[0].equals("DELETE_CAPTAIN")) {
                if (!words[0].equals("Quit")) {
                    fileWriter.write("Command " + words[0] + ":");
                }
            }

            switch (words[0]) {
                case "ADD_CAPTAIN":
                    tree.add_captain(Integer.parseInt(words[1]), words[2]);
                    break;
                case "BOOK_RIDE":
                    tree.book_ride(Integer.parseInt(words[1]));
                    break;
                case "DISPLAY_CAPTAIN_INFO":
                    tree.display_captain_by_ID(Integer.parseInt(words[1]));
                    break;
                case "FINISH_RIDE":
                    tree.finish_ride(Integer.parseInt(words[1]), Integer.parseInt(words[2]));
                    break;
                case "DISPLAY_ALL_CAPTAINS":
                    tree.display_all_captains_info();
                    break;
                case "DELETE_CAPTAIN":
                    tree.delete_captain(Integer.parseInt(words[1]));
                    break;
                case "Quit":
                    fileWriter.write(goodByePhrase);
                    fileWriter.flush();
                    fileWriter.close();
                    return;
            }
        }
    }

    private static boolean lineIsEmpty(String line) {
        return line.matches(" +") || line.isEmpty();
    }

}
