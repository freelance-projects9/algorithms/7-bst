import java.io.IOException;

public class UberTree {

    private CaptainNode root;


    void add_captain(int ID, String name) throws IOException {
        MainProgram.fileWriter.write(" Add a new captain record in the System\n\n");
        CaptainNode node = new CaptainNode(ID, name);
        if (isEmpty()) {
            root = node;
        } else {
            root.addChild(node);
        }
        node.displayInfo();
    }

    private boolean isEmpty() {
        return root == null;
    }

    void display_all_captains_info() throws IOException {
        writeLines(2);
        root.displayTreeInfo();
    }

    private void writeLines(int n) throws IOException {
        String lines = "";
        for (int j = 0; j < n; j++) {
            lines += "\n";
        }
        MainProgram.fileWriter.write(lines);
    }

    void display_captain_by_ID(int ID) throws IOException {
        CaptainNode captain = getCaptain(ID);
        if (captain == null) {
            MainProgram.fileWriter.write(" Couldn't find any captain with ID number " + ID + "\n\n" +
                    "----------------------------------------------------------------\n");
        } else {
            writeLines(1);
            captain.displayInfo();
        }
    }

    void book_ride(int ID) throws IOException {
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.fileWriter.write(" Couldn't find any captain with ID number " + ID + "\n\n" +
                    "----------------------------------------------------------------\n");
            return;
        } else if (node.isAvailable()) {
            node.book_captain();
            MainProgram.fileWriter.write(" Book a new Ride with captin " + ID + "\n\n" +
                    "----------------------------------------------------------------\n");
            return;
        }
        MainProgram.fileWriter.write(" The captain " + node.getName() + " is not available. He is on another ride!\n\n" +
                "----------------------------------------------------------------\n");
    }

    void finish_ride(int ID, int satisfaction) throws IOException {
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.fileWriter.write(" Couldn't find any captain with ID number " + ID + "\n\n" +
                    "----------------------------------------------------------------\n");
        } else if (node.isAvailable()) {
            MainProgram.fileWriter.write(" The  captain " + node.getName() + " is not in a ride!\n" +
                    "----------------------------------------------------------------\n");
        } else {
            node.rate(satisfaction);
            node.setAvailable(true);
            MainProgram.fileWriter.write(" Finish ride with captin " + ID + "\n\n");
            node.displayInfo();
        }
    }


    private CaptainNode successor(CaptainNode root) {
        root = root.getRight();
        while (root.getLeft() != null) {
            root = root.getLeft();
        }
        return root;
    }


    public CaptainNode deleteNode(CaptainNode root, int ID) {
        if (isEmpty()) return null;
        if (ID > root.getId()) {
            CaptainNode right = deleteNode(root.getRight(), ID);
            root.setRight(right);
        } else if (ID < root.getId()) {
            CaptainNode left = deleteNode(root.getLeft(), ID);
            root.setLeft(left);
        } else {
            root = doDelete(root);
        }
        return root;
    }

    private CaptainNode doDelete(CaptainNode root) {
        if (root.getLeft() == null && root.getRight() == null) { //hmm, its a leaf node; easy peasy
            root = null;
        } else if (root.getRight() != null) { // oh, it has a right child, don't make it an orphan or is it old enough to become a parent ? lets find out
            root.copy(successor(root)); // my worthy successor
            root.setRight(deleteNode(root.getRight(), root.getId()));
        } else { //oh it seems that I do not have a worthy successor, fallback, fallback ...
            root.copy(predecessor(root));
            root.setLeft(deleteNode(root.getLeft(), root.getId()));
        }
        return root;
    }

    private CaptainNode predecessor(CaptainNode root) {
        root = root.getLeft();
        while (root.getRight() != null) {
            root = root.getRight();
        }
        return root;
    }

    void delete_captain(int ID) throws IOException {
        CaptainNode node = getCaptain(ID);
        if (node == null) {
            MainProgram.fileWriter.write("Command DELETE_CAPTAIN: Couldn't find any captain with ID number " + ID + "\n\n" +
                    "----------------------------------------------------------------\n");
            return;
        }
        String name = node.getName();
        root = deleteNode(root, ID);
        MainProgram.fileWriter.write("Command  DELETE_CAPTAIN:The captain " + name + " left Uber\n" +
                "----------------------------------------------------------------\n");

    }

    /**
     * if (root == null) {
     * return;
     * }
     * CaptainNode temp2 = root;
     * CaptainNode temp;
     * <p>
     * <p>
     * while (temp2 != null) {
     * if (temp2.getRight().getId() == ID) {
     * if (temp2.getRight() != null) {
     * temp = temp2.getRight();
     * if (temp.getLeft() == null) {
     * temp.setLeft(temp2.getLeft());
     * root = temp;
     * break;
     * } else {
     * while (temp.getLeft().getLeft() != null) {
     * temp = temp.getLeft();
     * }
     * temp.getLeft().setLeft(temp2.getLeft());
     * temp.getLeft().setRight(temp2.getRight());
     * root = temp.getLeft();
     * temp.setLeft(null);
     * break;
     * }
     * } else {
     * root = temp2.getLeft();
     * break;
     * }
     * } else if (temp2.getId() < ID) {
     * temp2 = temp2.getRight();
     * } else {
     * temp2 = temp2.getLeft();
     * }
     * }
     *
     * @param ID
     * @return
     */

    CaptainNode getCaptain(int ID) {

        CaptainNode temp = root;
        while (temp != null) {
            if (ID == temp.getId()) {
                return temp;
            }
            if (ID > temp.getId()) {
                temp = temp.getRight();
            } else {
                temp = temp.getLeft();
            }
        }
        return null;
    }

}
